# PID Notebook

This project contains an interactive notebook to get a grasp of PI Controllers.

You can access it via Binder [here](https://binder.plutojl.org/v0.19.12/open?url=https%253A%252F%252Fgitlab.inria.fr%252Fcontrol-for-computing%252Fpid-notebook%252F-%252Fraw%252Fmaster%252Fpid.jl)
